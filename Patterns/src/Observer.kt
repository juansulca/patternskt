interface Observer {
    fun update(subject: Subject)
}

class Subject {
    
    var state = (0..10).random()
    
    private var observers = mutableListOf<Observer>()
    
    fun attach(observer: Observer) {
        observers.add(observer)
    }
    
    fun dettach(observer: Observer) {
        observers.remove(observer)
    }
    
    fun notifyObservers() {
        observers.forEach{ o -> o.update(this) }
    }
    
    fun businessLogic(){
        println("Executing some business logic") 
        state = (0..10).random()
        println("Changed state to: ${state}")
        notifyObservers()
    }
    
}

class ConcreteObserverA: Observer {
	override fun update(subject: Subject){
        if(subject.state < 3) {
            println("ConcreteObserverA: current state is ${subject.state}")
        }
    }    
}

class ConcreteObserverB: Observer {
	override fun update(subject: Subject){
        if(subject.state >= 3) {
            println("ConcreteObserverB: current state is ${subject.state}")
        }
    }    
}


fun main() {
    var subject = Subject()
    var obsA = ConcreteObserverA()
    var obsB = ConcreteObserverB()
    subject.attach(obsA)
    subject.attach(obsB)
    
    subject.businessLogic()
}