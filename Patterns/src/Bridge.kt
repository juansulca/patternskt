interface DataSerializer {
    fun serializeInt(key: String, value: Int): String
    fun serializeString(key: String, value: String): String
    fun serializeFloat(key: String, value: Float): String
}

class DataManager {
    private var dataSerializer: DataSerializer

    constructor(dataSerializer: DataSerializer) {
        this.dataSerializer = dataSerializer
    }

    fun manageInteger(key: String, value: Int) {
        println(dataSerializer.serializeInt(key, value))
    }

    fun manageString(key: String, value: String) {
        println(dataSerializer.serializeString(key, value))
    }

    fun manageFloat(key: String, value: Float) {
        println(dataSerializer.serializeFloat(key, value))
    }
}

class XMLSerializer: DataSerializer {
    override fun serializeInt(key: String, value: Int):String {
        return "<$key><int>$value</int></$key>"
    }
    override fun serializeString(key: String, value: String): String {
        return "<$key><string>$value</string></$key>"
    }
    override fun serializeFloat(key: String, value: Float): String {
        return "<$key><float>$value</float></$key>"
    }
}

class JSONSerializer: DataSerializer {
    override fun serializeInt(key: String, value: Int): String {
        return "$key: $value"
    }
    override fun serializeString(key: String, value: String): String {
        return "'$key': '$value'"
    }
    override fun serializeFloat(key: String, value: Float): String {
        return "'$key': $value"
    }
}

fun bridgeExample() {
    var json = JSONSerializer()
    var xml = XMLSerializer()
    var jsonManager = DataManager(json)
    var xmlManager = DataManager(xml)
    jsonManager.manageInteger("size", 5) 
    xmlManager.manageInteger("size", 5)
}