interface GhostFactory {

    fun create(): Ghost

    fun action(){
        val ghost = create()
        ghost.chase()
    }

}

interface Ghost {

    fun chase()

}

class Blinky: Ghost {
    override fun chase() {
        println("Chasing as Blinky...")
    }
}

class Clyde: Ghost {
    override fun chase() {
        println("Chasing as Clyde...")
    }
}

class BlinkyFactory: GhostFactory {
    override fun create(): Ghost {
        return Blinky()
    }
}

class ClydeFactory: GhostFactory {
    override fun create(): Ghost {
        return Clyde()
    }
}


fun main() {
    fun moveSprite(factory: GhostFactory) {
        factory.action()
    }

    moveSprite(BlinkyFactory())
    moveSprite(ClydeFactory())
}