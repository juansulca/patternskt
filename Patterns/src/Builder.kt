interface Builder {
    fun setSprite()
    fun setBehavior(behavior: String)
    fun setColor(color: String)
}

class Koopa {
    var sprite: String = ""
    var behavior: String = ""
    var color: String = ""
}

class Goomba {
    var sprite: String = ""
    var behavior: String = ""
    var color: String = ""
}

class KoopaBuilder: Builder {
    private var koopa = Koopa()

    override fun setSprite() {
        koopa.sprite = ".^.~"
    }
    override fun setBehavior(behavior: String) {
        koopa.behavior = behavior
    }
    override fun setColor(color: String) {
        koopa.color = color
    }

    fun getProduct(): Koopa {
        return koopa
    }

}

class GoombaBuilder: Builder {
    private var goomba = Goomba()
    override fun setSprite() {
        goomba.sprite = "(._.)"
    }
    override fun setBehavior(behavior: String) {
        goomba.behavior = behavior
    }
    override fun setColor(color: String) {
        goomba.color = "Brown"
    }
    fun getProduct(): Goomba{
        return goomba
    }
}

class Director {
    private var builder: Builder?

    constructor (builder: Builder){
        this.builder = builder
    }

    fun createGreenKoopa(){
        builder?.setSprite()
        builder?.setBehavior("Move forward, bump objects")
        builder?.setColor("Green")
    }

    fun createGoomba(){
        builder?.setSprite()
        builder?.setBehavior("Move forward, bump stuff")
        builder?.setColor("Brown")
    }
}

fun main() {
    var goombaBuilder = GoombaBuilder()
    var koopaBuilder = KoopaBuilder()
    var director = Director(goombaBuilder)
    director.createGoomba()
    var goomba = goombaBuilder.getProduct()
    println(goomba.sprite + " " + goomba.behavior + " " + goomba.color)
    director = Director(koopaBuilder)
    director.createGreenKoopa()
    var koopa = koopaBuilder.getProduct()
    println(koopa.sprite + " " + koopa.behavior + " " + koopa.color)
}