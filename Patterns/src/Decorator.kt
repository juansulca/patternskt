interface Field {

    public fun save()

}

class EmailField: Field {

    override fun save() {
        println("Saving EmailField...")
    }

}

open class FieldDecorator: Field {

    private var field: Field

    constructor(field: Field) {
        this.field = field
    }

    override fun save() {
        field.save()
    }
}

class EmailValidatioDecorator(field: Field): FieldDecorator(field) {
    override fun save() {
        println("Validating Email...")
        super.save()
    }
}

class SqlIjectionDecorator(field: Field): FieldDecorator(field) {
    override fun save() {
        println("Preventing SQL injection...")
        super.save()
    }
}

fun main(){
	var email = EmailField()
    email.save()
    
    println("==== Whith Decorators ====")
    var validation = EmailValidatioDecorator(email)
    var sqlInjection = SqlIjectionDecorator(validation)
    
    sqlInjection.save()
    
}