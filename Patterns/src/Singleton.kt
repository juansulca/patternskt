class Singleton private constructor(){
    
    init {
        instanceCount += 1
    }
    
    companion object {
        var instanceCount = 0
        private val instance: Singleton = Singleton()

        @Synchronized
        fun getInstance(): Singleton {
            return instance
        }
    }
    
    fun getInstanceCount(): Int {
            return instanceCount
    }
}

fun main(){
    var firstInstance = Singleton.getInstance()
    var secondInstance = Singleton.getInstance()

    println("FirstInstance: ${firstInstance.getInstanceCount()}")
    println("SecondInstance: ${secondInstance.getInstanceCount()}")
}